#+TITLE: Source Blocks for racket etc
#+INCLUDE: org-file-header-lines.org

*** Todo
    - citations
      - https://orgmode.org/manual/Citation-handling.html
      - https://orgmode.org/manual/Citation-export-processors.html
      - #+cite_export: basic author author-year
    - Index
      - https://orgmode.org/manual/Generating-an-index.html#Generating-an-index
      - 

*** Racket Source Block

#  https://orgmode.org/manual/Code-Evaluation-Security.html
# (defun my-org-confirm-babel-evaluate (lang body)
#   (not (string= lang "ditaa")))  ;don't ask for ditaa
# (setq org-confirm-babel-evaluate #'my-org-confirm-babel-evaluate)    


#+begin_src scheme :results
(require threading)
  
(~> 3 add1 sqrt)
#+end_src

#+RESULTS:
: 2


*** ditaa source block

# #+BEGIN_SRC ditaa :file images/hello-world-round.png :cmdline -r
# +--------------+
# |              |
# | Hello World! |
# |              |
# +--------------+
# #+END_SRC    
