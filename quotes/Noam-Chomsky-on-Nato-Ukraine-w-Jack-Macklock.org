#+INCLUDE: ../org-file-header-lines.org

#+begin_quote
“Since Putin’s major demand is an assurance that NATO will take no further members, and specifically not Ukraine or Georgia, obviously there would have been no basis for the present crisis if there had been no expansion of the alliance following the end of the Cold War, or if the expansion had occurred in harmony with building a security structure in Europe that included Russia.” ---Jack Macklock quoted by Noam-Chomsky
#+end_quote

#+INCLUDE: ../sources/Noam-Chomsky-tout-w-JackMacklock-Nato-Ukraine.org

