#+title: org-mode
#+author: Brian
#+date: 

To Rasmus, the most important aspects of /org-mode/ are:
- A nice syntax that's more powerful than /Markdown/, but easier than
  /LaTeX/.
- the excellent export framework, =ox.el=.
- The code evaluation, Babel or =ob.el=.

`org-mode` is distributed as part of [[https://www.gnu.org/s/emacs/][GNU Emacs]].

Brian thinks org-mode is great as a concise, powerful markup language too. The org-mode Markup is easier to remember and type than /LaTex/. I can't yet focus on my sentences and keep writing while using /Scribble/, but I wish I could use /org-mode/ to generate a bibliography and an index the way /Scribble/[fn:Scrbl] does.


[fn:Scrbl] [[https://bzmall2.gitlab.io/scriblog/index.html][https://bzmall2.gitlab.io/scriblog/index.html]]
